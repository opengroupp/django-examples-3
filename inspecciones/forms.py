from django.forms import ModelForm
from inspecciones.models import Centro, Vehiculo, Inspeccion

class CentroForm(ModelForm):
	class Meta:
		model = Centro
		exclude = ()


class VehiculoForm(ModelForm):
	class Meta:
		model = Vehiculo
		exclude = ()

class InspeccionForm(ModelForm):
	class Meta:
		model = Inspeccion
		exclude = ()