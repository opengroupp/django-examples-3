from __future__ import unicode_literals

from django.db import models


class Centro(models.Model):
	nombre = models.CharField(max_length=100)
	telefono = models.IntegerField()

	def __str__(self):
		return self.nombre


class Vehiculo(models.Model):
	nombre = models.CharField(max_length=100)
	modelo = models.CharField(max_length=100)


	def __str__(self):
		return self.nombre


class Inspeccion(models.Model):
	fecha = models.CharField(max_length=100)
	Centro = models.ForeignKey(Centro , on_delete = models.CASCADE)
	Vehiculo = models.ForeignKey(Vehiculo , on_delete = models.CASCADE)