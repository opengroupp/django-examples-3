from django.conf.urls import url, include
from inspecciones.views import ListaCentros, NuevoCentro, EditarCentro, EliminarCentro, NuevaInspeccion
from . import views
urlpatterns = [
   	


   	url(r'centros/$', ListaCentros.as_view(), name = 'listacentros'),
	url(r'centros/nuevo/$', NuevoCentro.as_view(), name = 'nuevocentro'),
	url(r'centro/editar/(?P<id>[0-9]+)/$', EditarCentro.as_view(), name = 'editarcentro'),
	url(r'centro/eliminar/$', EliminarCentro.as_view(), name = 'eliminarcentro'),
	url(r'vehiculos/$', views.lista_vehiculos, name='listavehiculos'),
	url(r'vehiculos/nuevo/$', views.nuevo_vehiculo, name = 'nuevovehiculo'),
	url(r'vehiculos/editar/(?P<id>[0-9]+)/$', views.editar_vehiculo, name = 'editarvehiculo'),
	url(r'vehiculos/eliminar/$', views.eliminar_vehiculo, name = 'eliminarvehiculo'),
	url(r'inspeccion/nuevo/$', NuevaInspeccion.as_view(), name = 'nuevainspeccion'),
	url('^$',views.index, name= 'index'),
   	url(r'registration/', include('django.contrib.auth.urls')),
	
	

]