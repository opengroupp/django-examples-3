from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.contrib.auth.views import redirect_to_login
from django.views import View
from inspecciones.models import Centro, Vehiculo, Inspeccion
from inspecciones.forms import CentroForm, VehiculoForm, InspeccionForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Sum

# Create your views here.


class ListaCentros(View):
	template_name = 'inspecciones/lista_centros.html'

	def get(self, request, *args, **kwargs):
		centros = Centro.objects.all()
		return render(request, self.template_name, { 'centros' : centros })


class NuevoCentro(View):
	template_name = 'inspecciones/editar_centro.html'
	form_class = CentroForm

	def get(self, request, *args, **kwargs):
		form = self.form_class()
		return render(request, self.template_name, { 'form' : form })

	def post(self, request, *args, **kwargs):
		form = self.form_class(request.POST)

		if form.is_valid():
			form.save()
			return redirect(reverse('inspecciones:listacentros'))

		return render(request, self.template_name, { 'form' : form })

	def dispatch(self, *args, **kwargs):
		if self.request.user.is_staff:
			return super(NuevoCentro, self).dispatch(*args, **kwargs)
		else:
			return redirect_to_login(reverse('inspecciones:nuevocentro'))




class EditarCentro(View):
	template_name = 'inspecciones/editar_centro.html'
	form_class = CentroForm

	def get(self, request, *args, **kwargs):
		c_id = kwargs['id']
		c = get_object_or_404(Centro, pk = c_id)
		if not c:
			return redirect(reverse('inspecciones:listacentros'))

		form = self.form_class(instance = c)
		return render(request, self.template_name, { 'form' : form })

	def post(self, request, *args, **kwargs):
		c_id = kwargs['id']
		c = get_object_or_404(Centro, pk = c_id)
		if not c:
			return redirect(reverse('inspecciones:listacentros'))
		form = self.form_class(request.POST, instance = c)

		if form.is_valid():
			form.save()
			return redirect(reverse('inspecciones:listacentros'))

		return render(request, self.template_name, { 'form' : form })

	def dispatch(self, *args, **kwargs):
		if self.request.user.is_staff:
			return super(EditarCentro, self).dispatch(*args, **kwargs)
		else:
			return redirect_to_login(reverse('inspecciones:editarcentro', kwargs = { 'id' : kwargs['id'] }))





class EliminarCentro(View):

	def get(self, request, *args, **kwargs):
		return redirect(reverse('inspecciones:listacentros'))

	def post(self, request, *args, **kwargs):
		c_id = request.POST.get('id')
		c = get_object_or_404(Centro, pk = c_id)
		c.delete()
		return redirect(reverse('inspecciones:listacentros'))

	def dispatch(self, *args, **kwargs):
		if self.request.user.is_staff:
			return super(EliminarCentro, self).dispatch(*args, **kwargs)
		else:
			return redirect_to_login(reverse('inspecciones:listacentros'))






def lista_vehiculos(request):
	template_name = 'inspecciones/lista_vehiculos.html'

	if request.method == 'GET':
		
		vehiculos = Vehiculo.objects.all()

		return render(request, template_name, { 'vehiculos' : vehiculos })



def nuevo_vehiculo(request):
	template_name = 'inspecciones/editar_vehiculo.html'
	form_class = VehiculoForm

	if not request.user.is_staff:
		return redirect_to_login(reverse('inspecciones:nuevovehiculo', kwargs = { 'idcir' : idcir }))

	cir = get_object_or_404(Centro)

	if request.method == 'POST':
		form = form_class(request.POST)

		if form.is_valid():
			form.save()
			return redirect(reverse('inspecciones:listavehiculos'))
	else:
		form = form_class(initial = { 'centro' : cir })
	return render(request, template_name, { 'form' : form })






def editar_vehiculo(request, id):
	template_name = 'inspecciones/editar_vehiculo.html'
	form_class = VehiculoForm

	if not request.user.is_staff:
		return redirect_to_login(reverse('inspecciones:editarvehiculo', kwargs = { 'id' : id }))

	vehiculo = get_object_or_404(Vehiculo, pk = id)
	if not vehiculo:
		return redirect(reverse('inspecciones:listavehiculos'))

	if request.method == 'POST':
		form = form_class(request.POST, instance = vehiculo)

		if form.is_valid():
			form.save()
			return redirect(reverse('inspecciones:listavehiculos'))
	else:
		form = form_class(instance = vehiculo)
	return render(request, template_name, { 'form' : form })





def eliminar_vehiculo(request):
	idvehiculo = request.POST.get('id')
	vehiculo = get_object_or_404(Vehiculo, pk = idvehiculo)


	if not request.user.is_staff:
		return redirect_to_login(reverse('inspecciones:listavehiculos'))

	if request.method == 'POST':
		vehiculo.delete()
		
	return redirect(reverse('inspecciones:listavehiculos'))




class NuevaInspeccion(View):
	template_name = 'inspecciones/nueva_inspeccion.html'
	form_class = InspeccionForm

	def get(self, request, *args, **kwargs):
		form = self.form_class()
		return render(request, self.template_name, { 'form' : form })

	def post(self, request, *args, **kwargs):
		form = self.form_class(request.POST)

		if form.is_valid():
			form.save()
			return redirect(reverse('inspecciones:listacentros'))

		return render(request, self.template_name, { 'form' : form })

	def dispatch(self, *args, **kwargs):
		if self.request.user.is_staff:
			return super(NuevaInspeccion, self).dispatch(*args, **kwargs)
		else:
			return redirect_to_login(reverse('inspecciones:nuevainspeccion'))





def index(request):
	template_name = 'inspecciones/index.html'
	
	return render(request, template_name)